unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.Layouts, FMX.ListBox, FMX.Controls.Presentation, FMX.StdCtrls;

type
  TForm1 = class(TForm)
    adicionar: TButton;
    atualizar: TButton;
    remover: TButton;
    salvar: TButton;
    carregar: TButton;
    listaplantas: TListBox;
    nome: TEdit;
    especie: TEdit;
    ambiente: TEdit;
    procedure adicionarClick(Sender: TObject);
    procedure removerClick(Sender: TObject);
    procedure atualizarClick(Sender: TObject);
    procedure salvarClick(Sender: TObject);
    procedure carregarClick(Sender: TObject);
  private
    { Private declarations }
  public
  type
    Tplanta = class(TObject)
      nome, especie, ambiente : string;
end;

    var planta: Tplanta;
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.adicionarClick(Sender: TObject);
begin
planta := Tplanta.Create;

if (nome.Text.Length<>0) and (especie.Text.Length<>0) and (ambiente.Text.Length<>0)  then begin

planta.nome := nome.Text;
  planta.especie := especie.Text;
  planta.ambiente := ambiente.Text;

  listaplantas.Items.AddObject(planta.nome, planta);
  end

  else begin
  ShowMessage('Campo obrigatório')

end;


end;

procedure TForm1.atualizarClick(Sender: TObject);
begin
planta:= listaplantas.Items.Objects[listaplantas.ItemIndex] as Tplanta;

if (nome.Text.Length<>0) and (especie.Text.Length<>0) and (ambiente.Text.Length<>0)  then begin

planta.nome := nome.Text;
  planta.especie := especie.Text;
  planta.ambiente := ambiente.Text;

  listaplantas.Items[listaplantas.ItemIndex] := nome.Text;
  end

  else begin
  ShowMessage('Campo obrigatório');

end;

end;

procedure TForm1.carregarClick(Sender: TObject);
begin
listaplantas.Items.LoadFromFile ('arquivo_plantas.txt');

end;

procedure TForm1.removerClick(Sender: TObject);
begin
listaplantas.Items.Delete(listaplantas.ItemIndex);

end;

procedure TForm1.salvarClick(Sender: TObject);

var texto : TextFile;
var i: integer;

begin

AssignFile(texto, 'arquivo_plantas.txt');

Rewrite(texto);
for i:=0 to Pred(listaplantas.Items.Count) do
      begin
        planta:= listaplantas.Items.Objects[i] as Tplanta;
        writeln(texto,'Planta ',i+1);
        writeln(texto,'Nome - ', planta.nome);
        writeln(texto,'Espécie - ',planta.especie);
        writeln(texto,'Ambiente - ',planta.ambiente);

end;
     CloseFile(texto);
end;

end.
